#!/usr/bin/env python
"""
SAGE VNC pointer

"""

# Source in this file based on Python VNC Viewer, Copyright (c) 2003 <cliechti@gmx.net> (MIT License---see below)
# Modifications for SAGE Copyright (c) 2014 Ian Peake (ian.peake@rmit.edu.au)

##########

# The MIT License (MIT)

# Copyright (c) 2003 <cliechti@gmx.net>

# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:

# The above copyright notice and this permission notice shall be included in
# all copies or substantial portions of the Software.

# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
# THE SOFTWARE.

##########

#twisted modules
from twisted.python import usage, log
from twisted.internet import reactor, protocol
#~ from twisted.internet import defer
from twisted.internet.protocol import Factory, Protocol

import pygame
from pygame.locals import *

import sys, struct
import trackapps

import rfb
import inputbox

import platform
import os

import pickle

import random

def seticon(iconname):
    """
    give an iconname, a bitmap sized 32x32 pixels, black (0,0,0) will be alpha channel
    
    the windowicon will be set to the bitmap, but the black pixels will be full alpha channel
     
    can only be called once after pygame.init() and before somewindow = pygame.display.set_mode()
    """
    icon=pygame.Surface((32,32))
    icon.set_colorkey((0,0,0))#and call that color transparant
    rawicon=pygame.image.load(iconname)#must be 32x32, black is transparant
    for i in range(0,32):
        for j in range(0,32):
            icon.set_at((i,j), rawicon.get_at((i,j)))
    pygame.display.set_icon(icon)#set wind

POINTER = tuple([(8,8), (4,4)] + list(pygame.cursors.compile((
#01234567
"        ", #0
"        ", #1
"    X   ", #2
"    X   ", #3
"  XX.XX ", #4
"    X   ", #5
"    X   ", #6
"        ", #7
), 'X', '.')))

#keyboard mappings pygame -> vnc
KEYMAPPINGS = {
    K_BACKSPACE:        rfb.KEY_BackSpace,
    K_TAB:              rfb.KEY_Tab,
    K_RETURN:           rfb.KEY_Return,
    K_ESCAPE:           rfb.KEY_Escape,
    K_KP0:              rfb.KEY_KP_0,
    K_KP1:              rfb.KEY_KP_1,
    K_KP2:              rfb.KEY_KP_2,
    K_KP3:              rfb.KEY_KP_3,
    K_KP4:              rfb.KEY_KP_4,
    K_KP5:              rfb.KEY_KP_5,
    K_KP6:              rfb.KEY_KP_6,
    K_KP7:              rfb.KEY_KP_7,
    K_KP8:              rfb.KEY_KP_8,
    K_KP9:              rfb.KEY_KP_9,
    K_KP_ENTER:         rfb.KEY_KP_Enter,
    K_UP:               rfb.KEY_Up,
    K_DOWN:             rfb.KEY_Down,
    K_RIGHT:            rfb.KEY_Right,
    K_LEFT:             rfb.KEY_Left,
    K_INSERT:           rfb.KEY_Insert,
    K_DELETE:           rfb.KEY_Delete,
    K_HOME:             rfb.KEY_Home,
    K_END:              rfb.KEY_End,
    K_PAGEUP:           rfb.KEY_PageUp,
    K_PAGEDOWN:         rfb.KEY_PageDown,
    K_F1:               rfb.KEY_F1,
    K_F2:               rfb.KEY_F2,
    K_F3:               rfb.KEY_F3,
    K_F4:               rfb.KEY_F4,
    K_F5:               rfb.KEY_F5,
    K_F6:               rfb.KEY_F6,
    K_F7:               rfb.KEY_F7,
    K_F8:               rfb.KEY_F8,
    K_F9:               rfb.KEY_F9,
    K_F10:              rfb.KEY_F10,
    K_F11:              rfb.KEY_F11,
    K_F12:              rfb.KEY_F12,
    K_F13:              rfb.KEY_F13,
    K_F14:              rfb.KEY_F14,
    K_F15:              rfb.KEY_F15,
}

MODIFIERS = {
    K_NUMLOCK:          rfb.KEY_Num_Lock,
    K_CAPSLOCK:         rfb.KEY_Caps_Lock,
    K_SCROLLOCK:        rfb.KEY_Scroll_Lock,
    K_RSHIFT:           rfb.KEY_ShiftRight,
    K_LSHIFT:           rfb.KEY_ShiftLeft,
    K_RCTRL:            rfb.KEY_ControlRight,
    K_LCTRL:            rfb.KEY_ControlLeft,
    K_RALT:             rfb.KEY_AltRight,
    K_LALT:             rfb.KEY_AltLeft,
    K_RMETA:            rfb.KEY_MetaRight,
    K_LMETA:            rfb.KEY_MetaLeft,
    K_LSUPER:           rfb.KEY_Super_L,
    K_RSUPER:           rfb.KEY_Super_R,
    K_MODE:             rfb.KEY_Hyper_R,        #???
    #~ K_HELP:             rfb.
    #~ K_PRINT:            rfb.
    K_SYSREQ:           rfb.KEY_Sys_Req,
    K_BREAK:            rfb.KEY_Pause,          #???
    K_MENU:             rfb.KEY_Hyper_L,        #???
    #~ K_POWER:            rfb.
    #~ K_EURO:             rfb.
}                        


class TextSprite(pygame.sprite.Sprite):
    """a text label"""
    SIZE = 20
    def __init__(self, pos, color = (255,0,0, 120)):
        self.pos = pos
        #self.containers = containers
        #pygame.sprite.Sprite.__init__(self, self.containers)
        pygame.sprite.Sprite.__init__(self)
        self.font = pygame.font.Font(None, self.SIZE)
        self.lastmsg = None
        self.update()
        self.rect = self.image.get_rect().move(pos)

    def update(self, msg=' '):
        if msg != self.lastmsg:
            self.lastscore = msg
            self.image = self.font.render(msg, 0, (255,255,255))

############    SAGE MESSAGING   ###################

MOVE = 1
CLICK = 2
WHEEL = 3
COLOR = 4
DOUBLE_CLICK = 5
POINT_ONLY = 6

LEFT = 1
RIGHT = 2
MIDDLE = 3  

# just used by the desktop mouse capture app
HAS_DOUBLE_CLICK = 10

# whether the pointer is visible or invisible...?
CAPTURED = 11 # visible(?)
RELEASED = 12 # invisible(?)

####################################################

from managerConn import ManagerConnection
from sageApp import SageApp

#~ class PyGameApp(pb.Referenceable, Game.Game):
class PyGameApp():
    """Pygame main application"""
    
    def __init__(self, dim, sageData, config):
        # clipboard init
        pygame.scrap.init()
        self.config = config
        # What SAGE app if any are we currently connected to (and sending key/mouse events)?
        # - if None, we are in "SAGE mode"
        self.currVncApp = None
        #self.setRFBSize(640, 400)
        # FIXME: this is a hack --- should not set/use vncWidth until actually connected
        self.width, self.height = pygame.display.get_surface().get_size()
        self.vncWidth, self.vncHeight = self.width, self.height
        pygame.display.set_caption('VNCPointer (SAGE)')
        pygame.mouse.set_cursor(*POINTER)
        pygame.key.set_repeat(500, 30)
        self.clock = pygame.time.Clock()
        self.alive = 1
        self.loopcounter = 0
        self.sprites = pygame.sprite.RenderUpdates()
        self.statustext = TextSprite((5, 0))
        self.sprites.add(self.statustext)
        self.buttons = 0
        self.protocol = None
        # SAGE pointer label - so we can modify it dynamically
        self.label = ""
        self.mode = ""
        # Config defaults
        saveDefaults = False
        if 'ptrColor' not in self.config:
          self.config['ptrColor'] = (int(random.random()*128)+128,int(random.random()*128),int(random.random()*128))
          log.msg( 'ptrColor defaults to '+str(self.config['ptrColor']) )
          saveDefaults = True
        if 'ptrLabel' not in self.config:
          self.config['ptrLabel'] = platform.node()
          log.msg( 'ptrLabel defaults to '+str(self.config['ptrLabel']) )
          saveDefaults = True
        if saveDefaults:
          saveConfig(self.config)  
        self.sageModePtrColor = self.config['ptrColor']
        self.setLabelText(self.config['ptrLabel'])
        ## in VNC? (if not, in SAGE)
        ##self.inApp = False
        self.connected = False
        # SAGE pointer position
        self.fx = 0
        self.fy = 0
        # SAGE
        self.dim = dim
        log.msg ('SAGE DIM '+str(self.dim))
        self.sendInitialMsg(self.dim, HAS_DOUBLE_CLICK)
        self.lastX = 0
        self.lastY = 0
        # what SAGE app is the pointer currently hovering over?
        self.sageApp = None
        self.sageData = sageData
        self.sageX = self.sageData.displayInfo.getTotalWidth()
        self.sageY = self.sageData.displayInfo.getTotalHeight()
        log.msg ('SAGE maxX, maxY '+str((self.sageX,self.sageY)))
        # Track previous event for multi-event matching
        self.prevE = None
        # Use fake window IDs to store VNC sessions not (yet) on SAGE desktop
        self.nextFakeWin = -2
        self.fullScreen = False
        # track keys currently pressed
        self.exitVNC = {}
        self.fsKeys = {}
        self.altTabKeys = {}
        self.ctrlAltDelKeys = {K_LSHIFT, K_LALT, K_LCTRL, K_d}
        self.pressedCtrlAltDelKeys = {}
        self.ctrlCKeys = {}
        self.ctrlVKeys = {}
        self.sageGateKeys = {}
        #
        self.screen = pygame.display.get_surface()
        self.setMode()
        self.vncRegReq = False
        #
        self.clientClipText = None

    # fake invisible app as a proxy for off-SAGE VNC service
    def fakeApp(self, url):
        sailId=0 # fake!
        zValue=-2 # lower than any sage app, even a temporary new one, so we are guaranteed to be above everything and deserve the focus
        orientation=0
        displayId=0 # fake!
        appId=0 # fake!
        launcherId=0 # fake!
        winId = self.nextFakeWin
        left=self.lastX
        top=self.lastY
        right=self.lastX+100
        bot=self.lastY-100
        fakeApp = SageApp(url, winId, left, right, bot, top, sailId, zValue, orientation, displayId, appId, launcherId, title="")
	self.nextFakeWin = self.nextFakeWin - 1
        return (winId,fakeApp)
       
    def setFactory(self, vncFactory):
        self.factory = vncFactory
    def setHost(self, host):
        self.host = host
    def setDisplay(self, display):
        self.display = display

    # direct color setting of SAGE pointer
    def setPointerColor(self, rgb):
	self.sendMsg(COLOR,str(rgb[0]),str(rgb[1]),str(rgb[2]))

    def tellPointerColor(self):
      if self.currVncApp:
        r,g,b = self.sageModePtrColor
        self.setPointerColor((r/3,g/3,b/3))
      else:
        self.setPointerColor(self.sageModePtrColor)

    # set SAGE-mode color
    def setSageModePointerColor(self, rgbtriple):
        self.sageModePtrColor = rgbtriple
        self.config['ptrColor'] = rgbtriple
        saveConfig(self.config)
        self.tellPointerColor()

    def setLabelText(self, labeltext):
        self.label = "%".join(labeltext.split())

    def tellSageLabel(self,newLabelText=None):
        # move pointer with old label somewhere less visible
        # multiple MOVES required to work around SAGE noisy input filtering?
	self.sendMsg(MOVE,self.fx/2,self.fy/2)
	self.sendMsg(MOVE,self.fx/8,self.fy/8)
	self.sendMsg(MOVE,self.fx/32,self.fy/32)
	self.sendMsg(MOVE,self.fx/1024,self.fy/1024)
        # create new pointer at same position
        # set mode text
        log.msg ('currVncApp '+str(self.currVncApp))
        log.msg ('protocol '+str(self.protocol))
        log.msg ('connected '+str(self.connected))
        if self.currVncApp:
          if self.connected:
            modetext="[A]"
          else:
            modetext="[A...]"
        else:
          modetext="[S]"
        self.mode = "%".join(modetext.split())
        # set label text
        if newLabelText!=None:
          self.setLabelText(newLabelText)
        self.sendInitialMsg(self.dim, HAS_DOUBLE_CLICK)
        if self.currVncApp:
          self.sendMsg(POINT_ONLY,"1")
        else:
          self.sendMsg(POINT_ONLY,"0")
        self.tellPointerColor()
	# move new pointer to correct position
        # multiple MOVES required to work around SAGE noisy input filtering?
	self.sendMsg(MOVE,15*self.fx/16,15*self.fy/16)
	self.sendMsg(MOVE,127*self.fx/128,127*self.fy/128)
	self.sendMsg(MOVE,511*self.fx/512,511*self.fy/512)
	self.sendMsg(MOVE,2047*self.fx/2048,2047*self.fy/2048)
	self.sendMsg(MOVE,self.fx,self.fy)

    def setMode(self):
        self.tellSageLabel()

    def setLabel(self,labelText):
        # update config first: safer than sending a message to SAGE
        self.config['ptrLabel'] = labelText
        saveConfig(self.config)
        self.tellSageLabel(newLabelText=labelText)

    def sendMsg(self, *data):
	msg = ""
	for m in data:
	    msg+=str(m)+" "
        if self.currVncApp: mode=" [A]"
        #if self.currVncApp:
	#  self.dim.sendMessage(self.label+self.mode, "pqlabs", msg)
        #else:
	self.dim.sendMessage(self.label+self.mode, "mouse", msg)

    def sendInitialMsg(self, manager, *data):
	msg = ""
	for m in data:
	    msg+=str(m)+" "
	#manager.initialMsg(self.label+mode, "mouse", msg)
	manager.initialMsg(self.label+self.mode, "mouse", msg)

    #  set up display to suit VNC connection
    #  params width, height, depth describe VNC server attrs
    def setRFBSize(self, width, height, depth=32):
        """change screen size"""
        # save true VNC width
        self.vncWidth, self.vncHeight = width, height
        log.msg ("setRFBSize "+str((width,height)))
        # set app dimensions if necessary
        if self.currVncApp and self.currVncApp.windowId<-1:
          log.msg( "Fake app... fill in window dimensions from VNC framebuffer "+self.currVncApp.toString() )
          self.currVncApp.right = self.currVncApp.left + self.vncWidth
          self.currVncApp.bottom = self.currVncApp.top - self.vncHeight
          log.msg( "- updated "+self.currVncApp.toString())
        # set to ``full screen''
        # FIXME: is this screen resizing needed here?
        self.screen = pygame.display.set_mode((0,0))
        self.width, self.height = pygame.display.get_surface().get_size()
        #print 'setRFBSize '+str((self.width,self.height))
        winstyle = 0 #FULLSCREEN
        if depth == 32:
            self.screen = pygame.display.set_mode((0,0), winstyle, 32)
        elif depth == 8:
            self.screen = pygame.display.set_mode((0,0), winstyle, 8)
            #default palette is perfect ;-)
            #~ pygame.display.set_palette([(x,x,x) for x in range(256)])
        #~ elif depth is None:
            #~ bestdepth = pygame.display.mode_ok((width, height), winstyle, 32)
            #~ print "bestdepth %r" % bestdepth
            #then communicate that to the protocol...
        else:
            raise ValueError, "color depth not supported"
        self.background = pygame.Surface((self.width, self.height), depth)
        self.background.fill(0) #black
        # Exit non-SAGE VNC session buttons
        self.exitVNC = {}

    def setProtocol(self, protocol):
        """attach a protocol instance to post the events to"""
        self.protocol = protocol

    def sageMouse(self,mx,my):
      oldApp = self.sageApp
      #print 'Raw ptr pos: '+str((mx,my))  
      # Convert laptop display x/y mouse position to range 0..1 for sage pointer (y is inverted)
      self.fx = float(mx)/self.width
      self.fy = 1-(float(my)/self.height)
      # SAGE mouse move
      self.sendMsg(MOVE, self.fx, self.fy)
      # Convert laptop display to co-ordinate in SAGE desktop
      #print 'SAGE desktop size: '+str((self.sageX,self.sageY))  
      x = int(self.fx*self.sageX)
      # x = self.mx*self.sageX/self.width
      y = int(self.fy*self.sageY)
      # y = (self.sageY - self.my)*self.sageY/self.height
      self.lastX = x
      self.lastY = y
      #print 'SAGE desktop ptr pos: '+str((x,y))  
      if self.sageApp!=None and self.sageApp.windowId<-1:
        # non-SAGE VNC mode: can't move mouse out (FIXME: what does this comment mean?)
        app = self.sageApp
        #print 'non-SAGE app: '+app.toString()
      else:
        app = self.sageData.checkHits(x, y)[0]
      #print 'checkHit app '+str(app)
      if app == None:
        if self.sageApp!=app:
          log.msg ('Pointer left app')
      else:
        self.appX = (x - app.left) * self.vncWidth / (app.right - app.left)
        self.appY = (app.top - y) * self.vncHeight / (app.top - app.bottom)
        #print 'x,y appx,appy'+str((x,y))+' '+str((self.appX,self.appY))
        #print 'top, bot, top-bot '+str((app.top,app.bottom,app.top-app.bottom))
        # limit app x,y to the application range
        if self.appX < 0: self.appX = 0
        if self.appX > self.vncWidth: self.appX = self.vncWidth
        if self.appY < 0: self.appY = 0
        if self.appY > self.vncHeight: self.appY = self.vncHeight
        if self.sageApp!=app:
          log.msg ('Ptr into window: ' + app.toString())
          log.msg ('SAGE x,y appx,appy'+str((x,y))+' '+str((self.appX,self.appY)))
          log.msg ('App top, bot, top-bot '+str((app.top,app.bottom,app.top-app.bottom)))
          wid = str(app.windowId)
          #print '- mapped VNC URL: '+str(self.getVncUrl(wid))
      self.sageApp = app
      if oldApp != self.sageApp:
        log.msg( 'sageApp = '+str(self.sageApp) )

    def getVncUrl(self,wid):
              if wid in self.sageData.vncMap:
                (host,displaystr) = self.sageData.vncMap[wid].split(':')
                display=int(displaystr)
                return (host,display)
              else:
                return None

# this is working.

    def checkEvents(self):
        """process events from the queue"""
        seen_events = 0
        for e in pygame.event.get():
            seen_events = 1
            # process mouse move events
            if e.type == MOUSEMOTION:
              self.sageMouse(e.pos[0],e.pos[1])
	    # try to force sageGate reconnect
	    if e.type == KEYDOWN and (e.key == K_LSHIFT or e.key == K_LCTRL or e.key == K_LALT or e.key == K_g):
	      self.sageGateKeys[e.key]=True
	      if K_LSHIFT in self.sageGateKeys and K_LCTRL in self.sageGateKeys and K_LALT in self.sageGateKeys and K_g in self.sageGateKeys:
		self.sageGateKeys={}
		log.msg ('try force reconnect to sageGate')
                log.msg ('- sageGate connected before? '+str(self.sageData.sageGate.connected))
                self.sageData = trackapps.trackApps(self.host)
		self.sageApp = None
		self.changeFocus()
	    else:
		self.sageGateKeys={}
	    # DISABLED: switch to/from full screen
	    if False: #e.type == KEYDOWN and (e.key == K_LSHIFT or e.key == K_LCTRL or e.key == K_LALT or e.key == K_f):
	      self.fsKeys[e.key]=True
	      log.msg ('full screen keys '+str(self.fsKeys))
	      if K_LSHIFT in self.fsKeys and K_LCTRL in self.fsKeys and K_LALT in self.fsKeys and K_f in self.fsKeys:
		log.msg ('toggle full screen')
		self.sageApp = None
		self.changeFocus()
		self.fsKeys={}
		self.fullScreen = not self.fullScreen
		if (self.fullScreen):
		  log.msg ('Enable fullscreen')
		  pygame.display.set_mode((0,0),pygame.FULLSCREEN)
		else:
		  log.msg ('Enable fullscreen')
		  pygame.display.set_mode()
	    else:
		self.fsKeys={}
            # SAGE mode
            if self.currVncApp==None:
              # QUIT - should be universal?
	      if e.type == QUIT or (e.type == KEYDOWN and e.key == K_ESCAPE):
		print 'QUIT'
		self.alive = 0
		if self.protocol != None:
		    self.protocol.transport.loseConnection()
                self.sendMsg(QUIT)
		reactor.stop()
		pygame.quit()
		# FIXME: should be able to leave cleanly without some of this?
		return 
              # manually set VNC URL
	      elif e.type == KEYDOWN and e.key == K_r and self.sageApp!=None:
		wid = str(self.sageApp.windowId)
		newURL=getHostURL() 
		log.msg( 'new URL '+str(newURL) )
		self.sageData.vncMap[wid]=str(newURL[0])+':'+str(newURL[1])
		log.msg( 'updated VNC URL map: '+str(self.sageData.vncMap) )
                self.changeFocus(vncRegReq=True)
              # manually enter VNC session not on SAGE desktop (visible from client only)
	      elif e.type == KEYDOWN and e.key == K_RETURN and self.sageApp==None:
		newURL=getHostURL() 
                display=(newURL[0])+':'+str(newURL[1])
                (wid,self.sageApp) = self.fakeApp(display)
		self.sageData.vncMap[str(wid)]=display
                self.changeFocus()
              # set pointer label
	      elif e.type == KEYDOWN and e.key == K_l:
		self.setLabel(getString("Pointer label"))
              # set pointer color
	      elif e.type == KEYDOWN and e.key == K_c:
		self.setSageModePointerColor(getColor())
              # buttons
	      elif e.type == MOUSEBUTTONUP:
		btnId = None
		if e.button == 1: btnId = LEFT
		if e.button == 2: btnId = MIDDLE
		if e.button == 3: btnId = RIGHT
		log.msg( 'SAGE: button up' )
		if btnId: self.sendMsg(CLICK,btnId,0)
	      elif e.type == MOUSEBUTTONDOWN:
		btnId = None
		if e.button == 1: btnId = LEFT
		if e.button == 2: btnId = MIDDLE
		if e.button == 3: btnId = RIGHT
		log.msg( 'SAGE: button down' )
		if btnId: self.sendMsg(CLICK,btnId,1)
            # VNC client mode
            # Process key/mouse events for current VNC session
            # TODO: optimise tests
            elif self.protocol != None:
              # exit VNC session - manual keypress
              if e.type == KEYDOWN and (e.key == K_LSHIFT or e.key == K_LCTRL or e.key == K_LALT or e.key == K_x):
		self.exitVNC[e.key]=True
		if K_LSHIFT in self.exitVNC and K_LCTRL in self.exitVNC and K_LALT in self.exitVNC and K_x in self.exitVNC:
		  print 'disconnect from VNC session'
		  self.sageApp = None
		  self.changeFocus()
		  self.exitVNC={}
	      else:
		  self.exitVNC={}
              # Send ALT-TAB
              if e.type == KEYDOWN and (e.key == K_LSHIFT or e.key == K_LCTRL or e.key == K_LALT or e.key == K_t):
		self.altTabKeys[e.key]=True
		if K_LSHIFT in self.altTabKeys and K_LCTRL in self.altTabKeys and K_LALT in self.altTabKeys and K_t in self.altTabKeys:
		  print 'sent ALT-TAB'
		  self.protocol.keyEvent(rfb.KEY_AltLeft, down=1)
		  self.protocol.keyEvent(rfb.KEY_Tab, down=1)
		  self.protocol.keyEvent(rfb.KEY_Tab, down=0)
		  self.protocol.keyEvent(rfb.KEY_AltLeft, down=0)
		  self.altTabKeys={}
              else:
                  self.altTabKeys={}
              # Send CTRL-ALT-DEL
              if e.type == KEYDOWN and e.key in self.ctrlAltDelKeys: #(e.key == K_LSHIFT or e.key == K_LCTRL or e.key == K_LALT or e.key == K_d):
		self.pressedCtrlAltDelKeys[e.key]=True
                if self.pressedCtrlAltDelKeys == self.ctrlAltDelKeys:
		  print 'set equality: CTRL-ALT-DEL'
		if K_LSHIFT in self.pressedCtrlAltDelKeys and K_LCTRL in self.pressedCtrlAltDelKeys and K_LALT in self.pressedCtrlAltDelKeys and K_d in self.pressedCtrlAltDelKeys:
		  print 'send CTRL-ALT-DEL'
		  self.protocol.keyEvent(rfb.KEY_ControlLeft, down=1)
		  self.protocol.keyEvent(rfb.KEY_AltLeft, down=1)
		  self.protocol.keyEvent(rfb.KEY_Delete, down=1)
		  self.protocol.keyEvent(rfb.KEY_Delete, down=0)
		  self.protocol.keyEvent(rfb.KEY_AltLeft, down=0)
		  self.protocol.keyEvent(rfb.KEY_ControlLeft, down=0)
		  self.pressedCtrlAltDelKeys={}
              else:
                  self.pressedCtrlAltDelKeys={}
              # Send CTRL-C
              if e.type == KEYDOWN and (e.key == K_LSHIFT or e.key == K_LCTRL or e.key == K_LALT or e.key == K_c):
		self.ctrlCKeys[e.key]=True
		if K_LSHIFT in self.ctrlCKeys and K_LCTRL in self.ctrlCKeys and K_LALT in self.ctrlCKeys and K_c in self.ctrlCKeys:
		  print 'send CTRL-C'
		  self.protocol.keyEvent(rfb.KEY_ControlLeft, down=1)
		  self.protocol.keyEvent(K_c, down=1)
		  self.protocol.keyEvent(K_c, down=0)
		  self.protocol.keyEvent(rfb.KEY_ControlLeft, down=0)
		  self.ctrlCKeys={}
              else:
                  self.ctrlCKeys={}
              # Send clipboard text if necessary
              if self.protocol != None:
                try:
                  text = pygame.scrap.get (SCRAP_TEXT)
                  # notify server of changed text and record notification
                  # - only if there is actually text
                  # - only if the text has changed since last notification
                  #log.msg('Current clipboard text at client: '+str(text))
                  #log.msg('Recorded previous clipboard text at client: '+str(self.clientClipText))
                  if text != None and self.clientClipText != text:
                    log.msg('New clipboard text at client: '+str(text))
                    self.protocol.clientCutText(text)
                    self.clientClipText = text
                except Exception as e:
                  log.msg('Clipboard operation exception: '+str(e))
              # Send CTRL-V
              if e.type == KEYDOWN and (e.key == K_LSHIFT or e.key == K_LCTRL or e.key == K_LALT or e.key == K_v):
		self.ctrlVKeys[e.key]=True
		if K_LSHIFT in self.ctrlVKeys and K_LCTRL in self.ctrlVKeys and K_LALT in self.ctrlVKeys and K_v in self.ctrlVKeys:
		  print 'send CTRL-V'
		  self.protocol.keyEvent(rfb.KEY_ControlLeft, down=1)
		  self.protocol.keyEvent(K_v, down=1)
		  self.protocol.keyEvent(K_v, down=0)
		  self.protocol.keyEvent(rfb.KEY_ControlLeft, down=0)
		  self.ctrlVKeys={}
              else:
                  self.ctrlVKeys={}
	      if e.type == KEYDOWN:
                  log.msg( "KEY DOWN: "+str(e)+' '+str(e.key) )
		  if e.key in MODIFIERS:
                      log.msg ("DOWN MOD "+str(e.key))
		      self.protocol.keyEvent(MODIFIERS[e.key], down=1)
		  elif e.key in KEYMAPPINGS:
                      log.msg ("DOWN KEY "+str(e.key))
		      self.protocol.keyEvent(KEYMAPPINGS[e.key], down=1)
                  # Control key combinations
                  elif (e.mod==64 or e.mod==320) and (e.key>=96 or e.key<=122):
                      log.msg ("DOWN CTRL char "+str(e.key))
		      self.protocol.keyEvent(e.key, down=1)
                  # FIXME: this clause is needed to send punctuation e.g "@"
                  # FIXME: this clause breaks sending of CTRL key combinations e.g. CTRL-C/CTRL-V
		  elif e.unicode:
                      log.msg ("DOWN Unicode")
		      self.protocol.keyEvent(ord(e.unicode), down=1)
		      self.protocol.keyEvent(ord(e.unicode), down=0)
		  else:
		      log.msg( "warning: unknown key DOWN %r" % (e) )
		      self.protocol.keyEvent(e.key, down=1)
	      elif e.type == KEYUP:
		  if e.key in MODIFIERS:
                      log.msg ("UP MOD")
		      self.protocol.keyEvent(MODIFIERS[e.key], down=0)
		  elif e.key in KEYMAPPINGS:
                      log.msg ("UP KEY")
		      self.protocol.keyEvent(KEYMAPPINGS[e.key], down=0)
                  elif (e.mod==64 or e.mod==320) and (e.key>=96 or e.key<=122):
                      log.msg ("UP CTRL char "+str(e.key))
		      self.protocol.keyEvent(e.key, down=0)
		  # FAILS with 'event member not defined'
                  # elif e.unicode:
                  #    log.msg ("UP Unicode")
		  #    self.protocol.keyEvent(ord(e.unicode), down=0)
		  else:
		      log.msg( "warning: unknown key UP %r" % (e) )
		      self.protocol.keyEvent(e.key, down=0)
	      elif e.type == MOUSEMOTION:
		    self.buttons  = e.buttons[0] and 1
		    self.buttons |= e.buttons[1] and 2
		    self.buttons |= e.buttons[2] and 4
		    self.protocol.pointerEvent(self.appX, self.appY, self.buttons)
	      # allow this to be unconditional so we can drag beyond the edge of the current window???
	      elif e.type == MOUSEBUTTONUP: 
		    if e.button == 1: self.buttons &= ~1
		    if e.button == 2: self.buttons &= ~2
		    if e.button == 3: self.buttons &= ~4
		    if e.button == 4: self.buttons &= ~8
		    if e.button == 5: self.buttons &= ~16
		    log.msg( 'MOUSEBUTTONUP' )
		    self.protocol.pointerEvent(self.appX,self.appY, self.buttons)
	      # only accept a click into VNC if the SAGE pointer is actually over the focus window
	      elif e.type == MOUSEBUTTONDOWN and self.currVncApp==self.sageApp:
		    if e.button == 1: self.buttons |= 1
		    if e.button == 2: self.buttons |= 2
		    if e.button == 3: self.buttons |= 4
		    if e.button == 4: self.buttons |= 8
		    if e.button == 5: self.buttons |= 16
		    log.msg( 'MOUSEBUTTONDOWN' )
		    self.protocol.pointerEvent(self.appX,self.appY, self.buttons)
              else:
                    log.msg ('Unknown event '+str(e))
            # Check for focus change (from either SAGE or VNC mode)
            # (NOTE:
            # - if doing this BEFORE SAGE/VNC mode processing, we lose a mouse-up event in the mode transition
            # - doing this AFTER SAGE/VNC mode processing, the last MOUSEBUTTONDOWN could go to the previous app)
            # Wait for Left MOUSE BUTTON UP immediately after Left MOUSE BUTTON DOWN
            if e.type == MOUSEBUTTONUP and e.button == 1 and self.prevE.type == MOUSEBUTTONDOWN and self.prevE.button == 1:
              log.msg( "MOUSEBUTTON DOWN+UP" )
              self.changeFocus()
            # record this event for multi-event matching
            self.prevE = e
        # end for (event e)
        return not seen_events

    def changeFocus(self,vncRegReq=False):
      self.vncRegReq=vncRegReq
      # force check connection to free space manager - TODO: make this asynchronous
      # - This appears to cause problems because it interrupts sageData class
      # - still trying to find a good nonce mechanism to test connect to free space manager
      ######self.sageData.sageGate.sendmsg('', 1000)
      self.sageData.sageGate.stopPerformance(0)
      # try resetting connection if need be - duplicated source code with key-initiated reconnected
      if not self.sageData.sageGate.isConnected() or self.sageData.sageGate.disconnected:
                log.msg ('sageGate disconnected - try force reconnect')
                self.sageData = trackapps.trackApps(self.host)
                self.sageApp = None
                self.changeFocus()
      else:
                log.msg ('sageGate still connected')
      log.msg( "Check for focuschange" )
      log.msg("VNC map: "+str(self.sageData.vncMap))
      log.msg("SAGE map: ")
      for app in self.sageData.hashAppStatusInfo:
	log.msg(" - "+self.sageData.hashAppStatusInfo[app].toString())
      log.msg("SAGE map end")
      if self.sageApp!=None:
        log.msg("mouse is over "+str(self.sageApp)+' '+self.sageApp.appName)
      else:
        log.msg("mouse is not over app")
      vncAble = True
      if self.sageApp != None:
        log.msg("known VNC app? winId is "+str(self.sageApp.windowId)+" "+str(self.sageData.vncMap))
        if str(self.sageApp.windowId) in self.sageData.vncMap and not (self.sageApp.appName.startswith('qshare') or self.sageApp.appName.startswith('decklink')):
          log.msg("assertion failure: current application is stale: not qshare/decklink but in VNC map: "+str(self.sageApp.appName)+" "+str(self.sageData.vncMap))
          vncAble = False
      # Click button outside of a SAGE app -> switch to SAGE mode
      # Click button in a SAGE app which has no VNC URL -> switch to SAGE mode
      # Click button in a SAGE app which has a non-VNC-able type -> switch to SAGE mode
      if (self.sageApp==None) or (not vncAble) or (str(self.sageApp.windowId) not in self.sageData.vncMap):
        # Switching to SAGE mode or in SAGE mode...
        self.connected = False
	# switch to "SAGE" mode
        if self.currVncApp!=None:
	  log.msg( "...new focus: SAGE mode")
	  self.currVncApp=None
	  self.setMode()
	  if self.protocol!=None:
	    self.protocol.transport.loseConnection()
        else:
          log.msg( "...staying in SAGE mode" )
      # Click button in a SAGE app which has known VNC URL...
      else:
	wid = str(self.sageApp.windowId)
	(host,displaystr) = self.sageData.vncMap[wid].split(':')
	display=int(displaystr)
	# different app to the currently-connected one (or SAGE mode) -> switch focus to that app
	if self.currVncApp!=self.sageApp:
	  log.msg( '...new focus (app changed): connecting to VNC service at SAGE window '+str(wid)+': '+str((host,displaystr)) )
	  self.host=host
	  self.display=display
	  reactor.connectTCP(
	    self.host,                             #remote hostname
	    display + 5900,                   #TCP port number
	    self.factory 
	  )
	  self.currVncApp = self.sageApp
	  self.setMode()
	# same app as current one... do nothing
	else:
	  log.msg("...same focus (app unchanged)")
	  pass
      log.msg ('SageGate connected ? '+str(self.sageData.sageGate.isConnected()))
      pass

    def mainloop(self, dum=None):
	"""gui 'mainloop', it is called repeated by twisteds mainloop by using callLater"""
        #~ self.clock.tick()
        no_work = self.checkEvents()

        #~ self.sprites.clear(self.screen, self.background)
        #~ dirty = self.sprites.draw(self.screen)
        #~ pygame.display.update(dirty)
        
        #~ self.statustext.update("iteration %d" % self.loopcounter)
        #~ self.loopcounter += 1
        
        #~ pygame.display.flip()
        
        if self.alive:
            #~ d = defer.Deferred()
            #~ d.addCallback(self.mainloop)
            #~ d.callback(None)
            reactor.callLater(no_work and 0.020, self.mainloop)
    
    #~ def error(self):
        #~ log.msg('error, stopping reactor')
        #~ reactor.stop()




class RFBToGUI(rfb.RFBClient):
    """RFBClient protocol that talks to the GUI app"""
    
    def vncConnectionMade(self):
        """choose appropriate color depth, resize screen"""
        #print "Screen format: depth=%d bytes_per_pixel=%r" % (self.depth, self.bpp)
        #print "Desktop name: %r" % self.name

        #print "redmax=%r, greenmax=%r, bluemax=%r" % (self.redmax, self.greenmax, self.bluemax)
        #print "redshift=%r, greenshift=%r, blueshift=%r" % (self.redshift, self.greenshift, self.blueshift)
        log.msg( 'RFBToGUI Connected')

        self.remoteframebuffer = self.factory.remoteframebuffer
        self.screen = self.remoteframebuffer.screen
        oldProtocol = self.remoteframebuffer.protocol
        self.remoteframebuffer.setProtocol(self)
        self.remoteframebuffer.setRFBSize(self.width, self.height, 32)
        if oldProtocol != None:
          log.msg( 'Closing previous connection')
          oldProtocol.transport.loseConnection()
        # 
        self.setEncodings(self.factory.encodings)
        self.setPixelFormat()           #set up pixel format to 32 bits
        #self.framebufferUpdateRequest() #request initial screen update
        app = self.remoteframebuffer.currVncApp
        # update pointer
        log.msg('Connected to VNC...')
        self.remoteframebuffer.connected = True
        self.clientCutText('Clipboard text from vncpointer')
        self.remoteframebuffer.setMode()
        # FIXME: refactor exception wrapper into trackapps.setVnc
        try:
          if self.remoteframebuffer.vncRegReq:
            log.msg("VNC registration requested ...")
            trackapps.setVnc(app.windowId,self.remoteframebuffer.sageData.vncMap[str(app.windowId)])
        except Exception as e:
          log.msg ('Exception '+str(e))

    def vncRequestPassword(self):
        if self.factory.password is not None:
            self.sendPassword(self.factory.password)
        else:
            #XXX hack, this is blocking twisted!!!!!!!
            #screen = pygame.display.set_mode((220,40))
            screen = pygame.display.get_surface()
            screen.fill((255,100,0)) #redish bg
            self.sendPassword(inputbox.ask(screen, "Password", password=1))
    
    #~ def beginUpdate(self):
        #~ """start with a new series of display updates"""

    def beginUpdate(self):
        """begin series of display updates"""
        #~ log.msg("screen lock")

    def commitUpdate(self, rectangles = None):
        """finish series of display updates"""
        #~ log.msg("screen unlock")
        #pygame.display.update(rectangles)
        #self.framebufferUpdateRequest(incremental=1)

    def updateRectangle(self, x, y, width, height, data):
        """new bitmap data"""
        #~ print "%s " * 5 % (x, y, width, height, len(data))
        #~ log.msg("screen update")
        #self.screen.blit(
        #    pygame.image.fromstring(data, (width, height), 'RGBX'),     #TODO color format
        #    (x, y)
        #)

    def copyRectangle(self, srcx, srcy, x, y, width, height):
        """copy src rectangle -> destinantion"""
        #~ print "copyrect", (srcx, srcy, x, y, width, height)
        #self.screen.blit(self.screen,
        #    (x, y),
        #    (srcx, srcy, width, height)
        #)

    def fillRectangle(self, x, y, width, height, color):
        """fill rectangle with one color"""
        #~ remoteframebuffer.CopyRect(srcx, srcy, x, y, width, height)
        #self.screen.fill(struct.unpack("BBBB", color), (x, y, width, height))

    def bell(self):
        log.msg( "BELL RING!" )

    def copy_text(self, text):
        log.msg( "Clipboard: %r" % text )
	pygame.scrap.put (SCRAP_TEXT, text)

#use a derrived class for other depths. hopefully with better performance
#that a single class with complicated/dynamic color conversion.
class RFBToGUIeightbits(RFBToGUI):
    def vncConnectionMade(self):
        log.msg( 'RFBToGUIeightbits: connected' )
        """choose appropriate color depth, resize screen"""
        self.remoteframebuffer = self.factory.remoteframebuffer
        self.screen = self.remoteframebuffer.screen
        self.remoteframebuffer.setProtocol(self)
        self.remoteframebuffer.setRFBSize(self.width, self.height, 8)
        self.setEncodings(self.factory.encodings)
        self.setPixelFormat(bpp=8, depth=8, bigendian=0, truecolor=1,
            redmax=7,   greenmax=7,   bluemax=3,
            redshift=5, greenshift=2, blueshift=0
        )
        self.palette = self.screen.get_palette()
        #self.framebufferUpdateRequest()

    def updateRectangle(self, x, y, width, height, data):
        """new bitmap data"""
        #~ print "%s " * 5 % (x, y, width, height, len(data))
        #~ assert len(data) == width*height
        #bmp = pygame.image.fromstring(data, (width, height), 'P')
        #bmp.set_palette(self.palette)
        #self.screen.blit(bmp, (x, y))

    def fillRectangle(self, x, y, width, height, color):
        """fill rectangle with one color"""
        #self.screen.fill(ord(color), (x, y, width, height))

class VNCFactory(rfb.RFBFactory):
    """A factory for remote frame buffer connections."""
    
    def __init__(self, remoteframebuffer, depth, fast, *args, **kwargs):
        rfb.RFBFactory.__init__(self, *args, **kwargs)
        # the pyGame app...
        self.remoteframebuffer = remoteframebuffer
        if depth == 32:
            self.protocol = RFBToGUI
        elif depth == 8:
            self.protocol = RFBToGUIeightbits
        else:
            raise ValueError, "color depth not supported"
            
        if fast:
            self.encodings = [
                rfb.COPY_RECTANGLE_ENCODING,
                rfb.RAW_ENCODING,
            ]
        else:
            self.encodings = [
                rfb.COPY_RECTANGLE_ENCODING,
                rfb.HEXTILE_ENCODING,
                rfb.CORRE_ENCODING,
                rfb.RRE_ENCODING,
                rfb.RAW_ENCODING,
            ]


    def buildProtocol(self, addr):
        log.msg("buildProtocol")
        display = addr.port - 5900
        pygame.display.set_caption('SAGE VNC Pointer - head node '+str(addr.host))
        return rfb.RFBFactory.buildProtocol(self, addr)

    def clientConnectionLost(self, connector, reason):
        message = "connection lost: %r" % reason.getErrorMessage()
        log.msg(message)
        self.remoteframebuffer.setMode()
        #reactor.stop()

    def clientConnectionFailed(self, connector, reason):
	message = "cannot connect to server: %r\n" % reason.getErrorMessage()
        log.msg(message)
        self.remoteframebuffer.currVncApp = None
        self.remoteframebuffer.protocol = None
        # this should already be false
        self.remoteframebuffer.connected = False
        self.remoteframebuffer.setMode()
        #reactor.stop()

class Options(usage.Options):
    optParameters = [
        ['display',     'd', '0',               'VNC display'],
        ['host',        'h', None,              'remote hostname'],
        ['outfile',     'o', None,              'Logfile [default: sys.stdout]'],
        ['password',    'p', None,              'VNC password'],
        ['depth',       'D', '32',              'Color depth'],
    ]
    optFlags = [
#        ['shared',      's',                    'Request shared session'],
        ['fast',        'f',                    'Fast connection is used'],
    ]

#~ def eventcollector():
    #~ while remoteframebuffer.alive:
        #~ pygame.event.pump()
        #~ e = pygame.event.poll()
        #~ if e.type != NOEVENT:
            #~ print e
            #~ reactor.callFromThread(remoteframebuffer.processEvent, e)
    #~ print 'xxxxxxxxxxxxx'

def getHostURL():
        screen = pygame.display.get_surface()
        screen.fill((0,100,255)) #blue bg
        host, display = None, -1
        while not(host and display>=0):
            url = inputbox.ask(screen, "Host:display")
            try:
	      host, displaystr = url.split(':')
	      if host == '':  host = 'localhost'
	      display = int(displaystr)
            except:
              pass
        return (host,display)

def getString(prompt):
        screen = pygame.display.get_surface()
        screen.fill((0,100,255)) #blue bg
	result = inputbox.ask(screen, prompt)
        return result

def getColor():
        screen = pygame.display.get_surface()
        screen.fill((0,100,255)) #blue bg
        (red,green,blue) = (None,None,None)
        while not (red and green and blue):
	  result = inputbox.ask(screen, "Color ``R,G,B''")
          redstr,greenstr,bluestr = result.split(",") 
          try:
            red,green,blue = int(redstr),int(greenstr),int(bluestr) 
          except:
            log.msg( 'Unable to convert '+((redstr,greenstr,bluestr)) )
        return (red,green,blue)

confFileName="vncpointer.conf"

# return key-value dict of config options
def loadConfig():
  if os.path.isfile(confFileName):
    result=pickle.load(open(confFileName,"rb"))
    #conffile=open(confFileName,"rb")
    #result=pickle.load(conffile)
    #fstats=os.fstat(conffile)
    #log.msg( 'inode#'+str(st_ino(fstats)))
    #conffile.close()
    log.msg( 'loaded config '+str(result) )
    return result
  else:
    return dict()

# save key-value dict of config options
def saveConfig(config):
  pickle.dump(config,open(confFileName,"wb"))

# try to find the absolute path name for the current script dir
# (to get co-located resources)
def setResourcePath():
  global resourcePath
  resourcePath = os.path.abspath(os.path.dirname(sys.argv[0]))
  log.msg( 'resource path = '+resourcePath )

cwd=os.getcwd()
print 'Current working directory '+str(cwd)

def main():
    config = loadConfig()

    setResourcePath()

    o = Options()
    try:
        o.parseOptions()
    except usage.UsageError, errortext:
        print "%s: %s" % (sys.argv[0], errortext)
        print "%s: Try --help for usage details." % (sys.argv[0])
        raise SystemExit, 1

    depth = int(o.opts['depth'])

    #if o.opts['outfile']:
    #    logFile = o.opts['outfile']

    f = open("vncpointer.log", "w")
    log.startLogging(f)

    #log.startLogging(sys.stdout)
    
    import os
    os.environ['SDL_VIDEO_WINDOW_POS'] = "%d,%d" % (0,0)
    pygame.init()
    seticon(os.path.join(resourcePath,'vncpointerIcon.bmp'))
    #pygame.key.set_repeat(0)
    #pygame.display.set_mode()
    pygame.display.set_mode((0,0))

    host = "10.234.2.30"
    if o.opts['host']:
      host = o.opts['host']
    display = 1
    if o.opts['display']:
      display = int(o.opts['display'])
    password = "sageuser"
    if o.opts['password']:
      password = o.opts['password']

    dimPort = 20005
    dimHost = host

    dim = ManagerConnection(dimHost,dimPort)
    sageData = trackapps.trackApps(host)
    remoteframebuffer = PyGameApp(dim,sageData,config)

    remoteframebuffer.setHost(dimHost)

    # eventually a set of VNC services with their parameters
    factory0 = VNCFactory(
	      remoteframebuffer,              #the application/display
	      depth,                          #color depth
	      o.opts['fast'],                 #if a fast connection is used
	      password,             	      #password or none
#	      int(o.opts['shared']),          #shared session flag
	      1,          		      #shared session flag
    )

    remoteframebuffer.setFactory(factory0)
    remoteframebuffer.setHost(host)
    remoteframebuffer.setDisplay(display)

    #~ from twisted.python import threadable
    #~ threadable.init()
    #~ reactor.callInThread(eventcollector)

    if host is None:
        (host,display)=getHostURL()

    # connect to this host and port, and reconnect if we get disconnected
    reactor.connectTCP(
        host,                                   #remote hostname
        display + 5900,                         #TCP port number
        factory0
    )

    # run the application
    reactor.callLater(0.02, remoteframebuffer.mainloop)
    reactor.run()


if __name__ == '__main__':
    main()
