import sageGateBase as sgb
from sageData import *
from twisted.python import usage, log
import time

sageHeadNode = ''

class MySageGate(sgb.SageGateBase):
    def __init__(self, host):
              self.host = host
              def disconnectFn():
                log.msg('sage gate unexpectedly disconnected')
                #time.sleep(20)
                # hack - don't completely trust SageGateBase
                self.disconnected = False
                #try:
                #  log.msg('reconnect'+str(self.isConnected()))
                #  self.connectToSage(self.host)
                #  log.msg('sage gate reconnected?'+str(self.isConnected()))
                #except:
                #  log.msg('sage gate failed to reconnect')
              sgb.SageGateBase.__init__(self,
                                  useAppLauncher=False, onDisconnect=disconnectFn) #, verbose=True)

    def onMessage(self, code, data):
        """ this is the function that gets called after a message arrives successfully
            it must be overridden by the subclass
        """
        log.msg( 'sageGate msg '+str((self, code, data) ) )
        if code in self.hashCallbackFunction:
            self.hashCallbackFunction[code](data)
        pass

def registerCallbacks(sageGate,sageData):
    # (AKS 2005-01-13)
    # Register callbacks from SageData with SageGate
    # Whenever SageGate receives an update message from SAGE,
    # the client-side "app database" stored in SageData
    # will be updated and then issue its own callback function
    # to tell the registering app to retrieve and process
    # the updated data
    #
    sageGate.registerCallbackFunction( 40000, sageData.setSageStatus )
    sageGate.registerCallbackFunction( 40001, sageData.setSageAppInfo )
    sageGate.registerCallbackFunction( 40002, sageData.setSagePerfInfo )
    sageGate.registerCallbackFunction( 40003, sageData.sageAppShutDown )
    sageGate.registerCallbackFunction( 40004, sageData.setDisplayInfo )
    sageGate.registerCallbackFunction( 40005, sageData.setSageZValue )
    sageGate.registerCallbackFunction( 40006, sageData.setSageAppExecConfig )
    sageGate.registerCallbackFunction( 40007, sageData.setDisplayConnections )

import urllib

# cache maps read from server
lastMap = {}

# Returns updated arg:result, a map from SAGE windowId strings to VNC display:port strings
def getVncApps(result):
  global sageHeadNode
  host = sageHeadNode
  # read VNC map from web service
  url = "http://"+str(host)+":8085/get-vnc-urls"
  print 'reading VNC map from '+url
  response = urllib.urlopen(url)
  html = response.read()
  lines=html.split("<br>")
  #print lines
  for line in lines:
    if line.startswith("WinID"):
      (marker, winId, url) = line.split()
      result[winId]=url
  print 'GetVncApps: '+str(result)
  global lastMap
  lastMap = result
  return result

# Add entry mapping string windowId arg:wid to string arg:vncurl (host:display string)
def setVnc(wid,vncurl):
  # don't add an existing entry
  global lastMap
  if wid in lastMap:
    print "not registering existing SAGE window "+str(wid)+", VNC url "+vncurl
    return
  global sageHeadNode
  host = sageHeadNode
  # set via web service
  serviceurl = "http://"+str(host)+":8085/set-vnc-url?wid="+str(wid)+"?display="+vncurl
  print "registering SAGE window "+str(wid)+", VNC url "+vncurl
  response = urllib.urlopen(serviceurl)
  html = response.read()
  print html

def trackApps(host):
  global sageHeadNode
  sageHeadNode = host
  print 'Setting up SAGE app tracking to '+str(host)
  sageGate = MySageGate(host)
  print 'gate '+str(sageGate)
  sageData = SageData(sageGate, False, 'SAGE test display name')
  registerCallbacks(sageGate,sageData)
  retVal = sageGate.connectToSage( host )
  print 'connectToSage = '+str(retVal)
  sageGate.registerSage()
  return sageData

