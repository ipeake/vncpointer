import time
from BaseHTTPServer import HTTPServer, BaseHTTPRequestHandler
from SocketServer import ThreadingMixIn
import threading
import subprocess
import os
import socket

#HOST_NAME = 'minnie'
#PORT_NUMBER = 9000

from socket import gethostname

PORT_NUMBER = 8085
HOST_NAME = 'minnie'

print 'Listen on '+str(HOST_NAME)+':'+str(PORT_NUMBER)

def run(cmd,file):
  file.write('<br>Running '+str(cmd))
  p = subprocess.Popen(cmd, stdout=subprocess.PIPE, close_fds=True)
  for line in p.stdout:
    file.write('<br>'+line)
    file.flush()

class MyHandler(BaseHTTPRequestHandler):
    def do_HEAD(s):
        s.send_response(200)

        s.send_header("Content-type", "text/html")
        s.end_headers()

    def do_GET(s):
        """Respond to a GET request."""
        s.send_response(200)
        s.send_header("Content-type", "text/html")
        s.end_headers()
        s.wfile.write("<html><head><title>VITELab Control Panel</title></head>")
        # If someone went to "http://something.somewhere.net/foo/bar/",
        # then s.path equals "/foo/bar/".

        message =  threading.currentThread().getName()
        s.wfile.write("<br>" +str(message))

        s.wfile.write("<p>You accessed path: %s</p>" % s.path)
        try:
          if s.path.startswith("/get-vnc-urls"):
            run(["touch","/var/tmp/sagevnc.log"],s.wfile)
            run(["cat","/var/tmp/sagevnc.log"],s.wfile)
            s.wfile.write("<br>done")
          elif s.path.startswith("/set-vnc-url?"):
            argstring=s.path[13:]
            args=argstring.split("?") 
            success=True
            if len(args)!=2:
              s.wfile.write("<br>Wrong number of args, expected 2 got "+str(len(args)))
              success=False
            if not (args[0].startswith("wid=") or args[1].startswith("display=")):
              s.wfile.write("<br>Usage: /set-vnc-url?wid=WID?display=HOST:PORT")
              success=False
            wid=args[0][:4]
            display=args[0][:9]
            run(["echo",'"WinID '+wid+" "+display+'" ',">>","/var/tmp/sagevnc.log"],s.wfile)
          else:
            s.wfile.write("unrecognized command")
          s.wfile.write("</body></html>")
        except Exception as e:
          s.wfile.write("<br>Exception "+str(e))

class ThreadedHTTPServer(ThreadingMixIn, HTTPServer):
    """Handle requests in a separate thread."""

if __name__ == '__main__':
    httpd = ThreadedHTTPServer((HOST_NAME, PORT_NUMBER), MyHandler)
    print time.asctime(), "Server Starts - %s:%s" % (HOST_NAME, PORT_NUMBER)
    try:
        httpd.serve_forever()
    except KeyboardInterrupt:
        pass
    httpd.server_close()
    print time.asctime(), "Server Stops - %s:%s" % (HOST_NAME, PORT_NUMBER)
