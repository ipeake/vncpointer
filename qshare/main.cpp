
#include "capturewindow.h"

#include <unistd.h>

int main(int argc, char *argv[])
{
    fprintf(stdout, "qshare.main\n");
    QApplication app(argc, argv);

    int errflg = 0;

    int c;
    while ((c = getopt(argc, argv, "cu:")) != -1)
    {
        switch (c)
        {
        case 'u': // hosting VNC display (host:port)
            vncurl = optarg;
            break;
        case 'c': // show cursor
            showCursor = true;
            fprintf(stdout, "Show cursor");
            break;
        default:
            errflg++;
            break;
        }
    }

    if (errflg)
    {
        fprintf(stderr, "\n");
        fprintf(stderr, "usage: %s\n", argv[0]);
        fprintf(stderr, "\t -u : hosting VNC display (host:port)\n");
        fprintf(stderr, "\t -c : show cursor\n");
        fprintf(stderr, "\t -h : this help\n");
        fprintf(stderr, "\n");
        exit(2);
    }

    CaptureWindow *widget = new CaptureWindow;

#if defined(__APPLE__)
	//widget->setWindowIcon();
#endif

    //widget->show();

    return app.exec();
}

