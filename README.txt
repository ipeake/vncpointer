vncpointer - keyboard/VNC-enhanced pointer for SAGE

Vncpointer behaves like sagePointer but with extensions to send
pointer/mouse events to desktops running VNC + qshare.

SAGE is the Scalable Adaptive Graphics Environment by UIC EVL (sagecommons.org).
SAGE is a middleware platform for high resolution tiled display walls.
If you don't use SAGE, this application is of no use to you.

Run "python vncviewer.py -h <hostname>" on a client machine e.g. desktop or laptop.

  <hostname> is the SAGE head node: SAGE fsManager, SAGE DIM must both be running here.

The default "SAGE" mode provides most standard SAGE features such as
move, resize, interaction with standard SAGE apps, clicking on SAGE desktop widgets, etc.

Any qshare app running on a VNC-enabled desktop can be registered as a "VNC app".
When you click on any VNC app, your key and mouse events are captured
and sent to that desktop via VNC ("VNC mode")

In VNC mode, when you are finished with some VNC app, click somewhere else.
Clicking in another registered VNC app switches focus to that VNC session.
Clicking anywhere else returns to SAGE mode.

To register a VNC app, move pointer over it in SAGE mode and press "R" key (for "register"),
and enter the <HOST:DISPLAY> string of the server
(at the moment I assume every VNC server has the password "sageusers").

On some occasions the client gets badly out-of-sync with the free space manager and
clicking with the pointer gives incorrect behaviours (ghost SAGE apps etc.).
Press CTRL-ALT-SHIFT-G to reset the connection and rebuild the local list of SAGE apps.

PREREQUISITES
- Working SAGE installation
- Compiled version of SAGE qshare (requires qshare patch to share VNC URLs)

There is a special linux script "sagebrowser" which runs a web browser on the wall which is pre-registered.
The sagebrowser runs an Xvnc (virtual X11 desktop) with firefox and a modified qshare which "pre-registers" VNC sessions.

Requires recompile of qshare to enable -u option and the additional "vncmap" web
service running on the same host as VNC+qshare (script writes a line into
/var/tmp/sagevnc.log, which is shared on request by the vncmap web service with
vncpointer when apps are updated)

To improve UI when in application mode, there are also some modifications to the direct interaction manager:
- copy mouse.py to <sage>/dim/devices/mouse.py
- copy app.py to <sage>/dim/overlays/app.py

BUGS

There are likely many bugs present. So far the most fundamental issue is that the association between SAGE qshare applications and their VNC URL is updated, stored and relayed outside the SAGE infrastructure, which increases complexity e.g. during installation and appears reduces the reliability / currency of the association data.

PROVENANCE

Based on code from SAGE and python VNC viewer (see separate LICENSES.txt).

TO RUN FROM SOURCE

use native python (e.g. under Windows install standard python) - recommended Python 2.7.3
Install dependencies:
- pygame
- twisted.python
- wxgtk

TO BUILD FROM SAGE

See SAGE instructions for building sagePointer in SAGE distribution.

FAQ

1. How do I install twisted on OS X Mavericks?

Use the instructions here:

http://stackoverflow.com/questions/22585393/using-twisted-on-os-x-mavericks

That is:

$ CFLAGS= pip install twisted
